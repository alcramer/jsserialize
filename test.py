import jsserialize

# test/dev code for jsserialize.
# We need some data to encode. We'll use a tree built from Nodes.
# The base class None has a "parent" attribute and a list "kids"
# containing child nodes. From the base class we define 2 subclasses,
# "A" and "B". The A class has a "color" attribute whose value is a string.
# The B class has a "date" attribute whose value is an object of class
# "date", from the standard python "datetime" module.

import datetime
nodeid = 0

class Node():
    def __init__(self):
        global nodeid
        self.id = str(nodeid)
        nodeid += 1
        self.parent = None
        self.kids = []

    def add_kid(self,kid):
        self.kids.append(kid)
        kid.parent = self

    def __str__(self):
        pid = "None" if self.parent is None else self.parent.id
        return ('%s %s parent:%s' %
            (self.__class__.__name__, self.id,pid))

    def value_to_str(self,v):
        if isinstance(v,dict):
            entries = []
            for key,v in v.items():
                if key in ['parent','kids']:
                    continue
                entries.append('%s:%s' % (key,self.value_to_str(v)))
            return '{%s}' % ' '.join(entries)
        if isinstance(v,list) or isinstance(v,tuple):
            entries = [self.value_to_str(e) for e in v]
            return '[%s]' % ' '.join(entries)
        return v.__str__()

    def printme(self,indent=''):
        print('%s%s %s' %
            (indent,self.__str__(), self.value_to_str(self.__dict__)))
        indent += '  '
        for k in self.kids:
            k.printme(indent)

class A(Node):
    def __init__(self,color='red'):
        super().__init__()
        self.color = color

class B(Node):
    def __init__(self,date=None):
        super().__init__()
        self.date = date

# make the data we'll encode/decode: a tree built from A and B nodes.
def make_data():
    root = A()
    kid1 = A('yellow')
    kid2 = B(datetime.date.fromordinal(1))
    root.add_kid(kid1)
    root.add_kid(kid2)
    kid2.add_kid( A('blue') )
    kid2.add_kid( B(datetime.date.today()) )
    # add a dynamic attribute to kid2: a dict with entry ref'ing
    # kid1
    kid2.mydct = {'sister':kid1}
    return root

# Here are the custom encode/decode functions. We need them
# because our data includes objects of class "date"
# from the python standard "datetime" module, and jsserialize
# doesn't know how to encode/decode objects of that class.
# Note that these encode/decode functions only need to handle
# "date" objects -- our data also includes objects of class "A"
# and "B", but jsserialize can handle them without our help. (For this
# to work, we have to make sure our user-defined classes have
# default constructors).

def custom_encode(obj):
    if obj.__class__.__name__ == 'date':
        # 'obj.isoformat()' returns a str
        return {'isoformat': obj.isoformat()}
    # returning None tells jsserialize: we didn't encode the
    # object, you do it.
    return None

def custom_decode(classname,dct):
    # Notice that classname is the "full" name for the class:
    # module name, followed the name that appeared in the
    # class definition.
    if classname == 'datetime.date':
        # "fromisoformat" is class method that creates
        # a date object from an isoformat string
        return datetime.date.fromisoformat(dct['isoformat'])
    # returning None tells jsserialize: we didn't decode the
    # object, you do it.
    return None

def test():
    # create data to be encoded: a tree built from nodes
    data = make_data()
    print("data to be encoded:")
    data.printme()

    # get the json string encoding
    json_str = jsserialize.encode(data,custom_encode=custom_encode)

    # decode the json_str, recreating the data
    data2 = jsserialize.decode(json_str, custom_decode=custom_decode)
    print("\ndecoded data:")
    data2.printme()

    # print the json string
    print("\njson string:")
    print(json_str)

if __name__=='__main__':
    test()


